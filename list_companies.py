import csv
import os
import re

from linkedin_scraper import Company, CompanyFilter, actions
from linkedin_scraper.utils import webdriver
from pprint import pprint

try:
  driver = webdriver.resume_session()
  driver.get("https://www.linkedin.com")
except:
  driver = webdriver.start_session()

actions.login(driver, os.environ["LINKEDIN_USERNAME"], os.environ["LINKEDIN_PASSWORD"])

company_filter = CompanyFilter(driver)
filter_keys = ["food & beverages", "wine and spirits"]
i = 0

for filter_key in filter_keys:
  company_filter.apply_filter(filter_key)
  name = re.sub(r'\s+', '_', filter_key)
  name = re.sub(r'\W', '', name)
  dest = "./data/%s" % name

  if not os.path.exists(dest):
    os.mkdir(dest)

  companies_csv = os.path.join(dest, "companies.csv")

  with open(companies_csv, "a") as f:
    writer = csv.writer(f, delimiter=',', quotechar='"')

    if os.stat(companies_csv).st_size == 0:
      writer.writerow(["name", "href"])

    while True:
      for item in company_filter.extract_results():
        writer.writerow([item["name"], item["href"]])

      if not company_filter.next_page():
        break

    i += 1
    print("Done: %d-%s" % (i, filter_key))
