import time
import os
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from .utils import interaction
from .objects import Scraper
from .person import Person
from .utils import webdriver

class CompanyFilter(Scraper):
    def __init__(self, driver=None):
        self.driver = webdriver.resolve_driver() if driver is None else driver

    def apply_filter(self, filter_key):
        self.driver.get("https://www.linkedin.com/search/results/companies/")
        dropdown_id = "hoverable-outlet-industry-filter-value"
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, dropdown_id)))

        # open filter
        self.driver.find_element_by_css_selector("#%s + .artdeco-hoverable-trigger > button" % dropdown_id).click()

        # check option
        self.driver.find_element_by_css_selector("#%s .search-basic-typeahead input" % dropdown_id).send_keys(filter_key)
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "basic-typeahead__triggered-content")))
        time.sleep(3)
        self.driver.find_element_by_css_selector("#%s .basic-typeahead__triggered-content" % dropdown_id).click()

        # submit filter
        self.driver.find_element_by_css_selector("#%s .artdeco-button--primary" % dropdown_id).click()
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "search-marvel-srp")))

    def extract_results(self):
        name_anchors = self.driver.find_elements_by_css_selector(".search-marvel-srp .entity-result__title-text a")
        data = []

        for a in name_anchors:
            data.append({ "href": a.get_property("href"), "name": a.text })

        return data

    def next_page(self):
        WebDriverWait(self.driver, 300).until(EC.presence_of_element_located((By.CLASS_NAME, "search-marvel-srp")))
        interaction.scroll_to_bottom(self.driver)

        next_btn = self.driver.find_element_by_class_name("artdeco-pagination__button--next")
        has_next_page = not next_btn.get_property("disabled")

        if has_next_page:
            next_btn.click()
            WebDriverWait(self.driver, 300).until(EC.presence_of_element_located((By.CLASS_NAME, "search-marvel-srp")))

        return has_next_page
