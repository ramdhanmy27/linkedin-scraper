import time

def scroll_to_bottom(driver, scale=1, sleep_time=1):
  driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight*%.2f));" % scale)
  time.sleep(sleep_time)

def scroll_to_top(driver, sleep_time=1):
  driver.execute_script("window.scrollTo(0, 0);")
  time.sleep(sleep_time)

def scroll_to_ele(driver, ele, sleep_time=1):
  driver.execute_script("window.scrollTo(0, arguments[0].offsetTop)", ele)
  time.sleep(sleep_time)
