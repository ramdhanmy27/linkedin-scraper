import os
import json
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By

def resume_session(cache_location="./cache/webdriver.json"):
  # try reading session data from cache if exists
  if os.path.exists(cache_location):
    with open(cache_location) as f:
      session = json.load(f)
      driver = webdriver.Remote(command_executor=session["url"], desired_capabilities={})
      driver.close() # this prevents the dummy browser
      driver.session_id = session["id"]

      return driver

def start_session(cache_location="./cache/webdriver.json"):
  driver = webdriver.Chrome()

  # save current session data
  with open(cache_location, "w") as f:
    json.dump({
      "url": driver.command_executor._url,
      "id": driver.session_id,
    }, f)

  return driver

def resolve_driver():
  try:
    if os.getenv("CHROMEDRIVER") == None:
      driver_path = os.path.join(os.path.dirname(__file__), 'drivers/chromedriver')
    else:
      driver_path = os.getenv("CHROMEDRIVER")

    return webdriver.Chrome(driver_path)
  except:
    return webdriver.Chrome()

def wait_or_refresh(driver, by, selector, timeout=20, repeat_until=2, iteration=1):
  try:
    WebDriverWait(driver, timeout).until(EC.presence_of_element_located((by, selector)))
  except:
    if repeat_until > iteration:
      driver.refresh()
      wait_or_refresh(driver, by, selector, timeout/2, iteration=iteration + 1)
