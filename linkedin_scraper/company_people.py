import os
import re
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from .utils import interaction
from .objects import Scraper
from .person import Person
from .utils import webdriver


class CompanyPeople(Scraper):
  url = None
  people_panel = None

  def __init__(self, driver):
    self.driver = webdriver.resolve_driver() if driver is None else driver

  def apply_region_filter(self, region):
    pass

  def scrape(self, url, group_by_regions=True):
    self.url = url
    data = {}

    # overview
    self.driver.get(os.path.join(url, "about"))
    data["about"] = self.driver.find_element_by_css_selector(".artdeco-card p.break-words").text
    data["name"] = self.driver.find_element_by_class_name("org-top-card-summary__title").text

    overview_table = self.driver.find_element_by_css_selector(".artdeco-card dl")
    data["industry"] = overview_table.find_element_by_css_selector("dd:nth-of-type(2)").text
    data["headquarters"] = overview_table.find_element_by_css_selector("dd:nth-of-type(5)").text
    data["type"] = overview_table.find_element_by_css_selector("dd:nth-of-type(6)").text

    # people
    self.driver.get(os.path.join(url, "people"))
    self.people_panel = self.__get_people_panel()
    time.sleep(1)

    self.__show_more()
    self.__next_page()

    total_employees = self.people_panel.find_element_by_css_selector("span:nth-of-type(1)").text
    data["total_employees"] = re.sub("\D", "", total_employees)
    data["functions"] = self.__extract_from_graph_module_by_name("current-function")

    self.__prev_page()

    # filtered by regions
    if group_by_regions:
      regions = self.__extract_from_graph_module_by_name("geo-region")
      data["regions"] = []

      i = 2
      for region in regions:
        self.__show_more()

        try:
          # click a region
          region_btn_sel = ".org-people-bar-graph-module__geo-region .org-people-bar-graph-element--is-selectable:nth-of-type(%d)" % i
          region_btn = self.people_panel.find_element_by_css_selector(region_btn_sel)
          interaction.scroll_to_ele(self.driver, region_btn, 0)
          region_btn.click()

          webdriver.wait_or_refresh(self.driver, By.CSS_SELECTOR, region_btn_sel)
        except Exception as e:
          print("# filter failed: %d" % i)

        region_info = self.people_panel.find_element_by_css_selector(
          region_btn_sel + " .org-people-bar-graph-element__percentage-bar-info"
        )
        region_name = region_info.find_element_by_tag_name("span").text
        region_total_employees = re.sub("\D", "", region_info.find_element_by_tag_name("strong").text)

        time.sleep(1)
        self.__next_page()

        data["regions"].append({
          "name": region_name,
          "total_employees": region_total_employees,
          "functions": self.__extract_from_graph_module_by_name("current-function"),
        })

        self.__clear_filter()
        i += 1

    return data

  def __show_more(self):
    webdriver.wait_or_refresh(self.driver, By.CLASS_NAME, "org-people__show-more-button")

    try:
      show_more_btn = self.people_panel.find_element_by_xpath("//*[contains(@class, 'org-people__show-more-button') and @aria-expanded='false']")
      interaction.scroll_to_ele(self.driver, show_more_btn, 0)
      show_more_btn.click()
    except:
      pass

  def __extract_from_graph_module_by_name(self, graph_module_name):
    graph_module = self.people_panel.find_element_by_class_name("org-people-bar-graph-module__%s" % graph_module_name)
    return self.__extract_from_graph_module(graph_module)

  def __extract_from_graph_module(self, graph_module_el):
    self.__show_more()
    interaction.scroll_to_ele(self.driver, graph_module_el, 0)

    graph_els = graph_module_el.find_elements_by_class_name("org-people-bar-graph-element--is-selectable")
    data = []

    for graph_el in graph_els:
      interaction.scroll_to_ele(self.driver, graph_el, 0)
      graph_info = graph_el.find_element_by_class_name("org-people-bar-graph-element__percentage-bar-info")

      data.append({
        "name": graph_info.find_element_by_tag_name("span").text,
        "value": re.sub("\D", "", graph_info.find_element_by_tag_name("strong").text),
      })

    return data

  def __clear_filter(self):
    try:
      self.people_panel.find_element_by_css_selector(".org-people__insights-container ul.list-style-none li:last-child button").click()
      webdriver.wait_or_refresh(self.driver, By.CLASS_NAME, "org-people-bar-graph-module__geo-region")
    except:
      # force reset filter
      self.driver.get(os.path.join(self.url, "people"))
      webdriver.wait_or_refresh(self.driver, By.CLASS_NAME, "org-people-bar-graph-module__geo-region")
      self.people_panel = self.__get_people_panel()

  def __get_people_panel(self):
    return self.driver.find_element_by_css_selector(".org-grid__content-height-enforcer .artdeco-card")

  def __next_page(self):
    next_btn = self.people_panel.find_element_by_class_name("artdeco-pagination__button--next")
    interaction.scroll_to_ele(self.driver, self.people_panel, 0)
    next_btn.click()
    time.sleep(1)

  def __prev_page(self):
    prev_btn = self.people_panel.find_element_by_class_name("artdeco-pagination__button--previous")
    interaction.scroll_to_ele(self.driver, self.people_panel, 0)
    prev_btn.click()
    time.sleep(1)
