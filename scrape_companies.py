import csv
import os
import re
import pandas as pd
import logging
from pprint import pprint

from linkedin_scraper import Company, CompanyPeople, actions
from linkedin_scraper.utils import webdriver

logging.basicConfig(filename="history.log", filemode='a', level=logging.INFO)

try:
  driver = webdriver.resume_session()
  driver.get("https://www.linkedin.com")
except:
  driver = webdriver.start_session()

actions.login(driver, os.environ["LINKEDIN_USERNAME"], os.environ["LINKEDIN_PASSWORD"])

company_people = CompanyPeople(driver)
data_dir = "./data"

for category in os.listdir(data_dir):
  dirpath = os.path.join(data_dir, category)
  companies_csv_path = os.path.join(dirpath, "companies.csv")

  if os.path.exists(companies_csv_path):
    data_csv_path = os.path.join(dirpath, "data.csv")

    with open(data_csv_path, "a") as f:
      writer = csv.writer(f, delimiter=',', quotechar='"')

      if os.stat(data_csv_path).st_size == 0:
        writer.writerow([
          "name",
          "url",
          "about",
          "industry",
          "headquarters",
          "type",
          "total_employees",
          "functions",
          "regions",
        ])

      checkpoint = None

      # iterate over through company list loaded from csv
      for chunk in pd.read_csv(companies_csv_path, chunksize=10**6):
        for i, row in chunk.iterrows():
          if checkpoint is not None and checkpoint > i:
            print("[SKIPPED] #%d %s" % (i + 1, row['href']))
            continue

          try:
            # extract data from company's profile page
            data = company_people.scrape(row['href'], group_by_regions=True)

            # write results into csv
            writer.writerow([
              data["name"],
              row['href'],
              data["about"],
              data["industry"],
              data["headquarters"],
              data["type"],
              data["total_employees"],
              data["functions"],
              data["regions"],
            ])

            print("#%d %s" % (i + 1, row['href']))
          except Exception as e:
            logging.info("[FAILED] #%d %s" % (i + 1, row['href']))
            logging.error(e)

    print("Done: %s" % category)
